Comala Workflows API Documentation:
============================================

This API was deciphered out of Comala class objects and based on different tests.
It tries to help the Comala API knowledge for future development.

Latest update: 2016.10.04

Class ApprovalService:
import com.comalatech.workflow.ApprovalService;
  Approval approve(AbstractPage page, String, String, String, String);
  Rejection reject(AbstractPage page, String, String, String);
  assign(AbstractPage page, String, String, String);
  unassign(AbstractPage page, ?);
  Iterator getAssigned(Set, String);
  Iterator<ContentTask> signature(Set<String>, String);

Class StateService:
import com.comalatech.workflow.StateService;
  State getPublishedState(AbstractPage page);
  State getCurrentState(AbstractPage page);
  State getCurrentApprovals(AbstractPage page);

Class TaskService:
  import com.comalatech.workflow.TaskService;
  List<? extends ContentTask> getCompletedTasks(AbstractPage page);

Class WorkflowService:
  import com.comalatech.workflow.WorkflowService;
  List getActiveWorkflows();

Class Activity:
import com.comalatech.workflow.model.Activity;

Class State: extends Activity:
import com.comalatech.workflow.model.State;
  List<? extend Assignment> getAssignments();
  List<? extends Approval> getApprovals();

Class Assignment:
import com.comalatech.workflow.model.Assignment;

Class Approval:
import com.comalatech.workflow.model.Approval;
  String getName();
  Date getDate();

Class ContentTask:
import com.comalatech.workflow.model.ContentTask;
  Task getTask();
  Object getContent();

Class Task:
import com.comalatech.workflow.model.Task;
  String getName();
  List<? extends Assignment> getAssignments();
  List Signature();
  List<? extends Completion> getCompletion();
  boolean getOutcomeIsCompleted();
