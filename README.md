# Comala Macro Plugin

This plugin provides a base to develop plugins using Comala on Confluence.

# License

This plugin is licensed under Apache License 2.0

# How to build

- Debian
  1. Create the jdk package and install it:
  See: https://wiki.debian.org/JavaPackage
  `apt-get install java-package`
  Download the Java jdk corresponding to you Bitbucket server version: http://www.oracle.com/technetwork/java/javase/downloads/index.html
  as user: `fakeroot make-jpkg ./jdk-myversion.tar.gz`
  Once done: install the corresponding Debian package created, as root: `dpkg -i ./jdk-myversion.deb`

  2. Install Atlassian developer tools for Debian
  Check you bitbucket version and download the corresponding deb package: https://marketplace.atlassian.com/plugins/atlassian-plugin-sdk-deb/versions

  3. Install Confluence, the Comala workflow plugin, and maven.

  4. Clone this repository and build the atlassian package
  `git clone repository/path`  
  `cd repository/path`  
  `atlas-package`  

Full documentation is always available at:  
- https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK
- https://developer.atlassian.com/confdev/tutorials/macro-tutorials-for-confluence/creating-a-new-confluence-macro
- https://wiki.comalatech.com/display/CW/API+Getting+Started

# Server-side configuration:

- Use the 'Manage Add-ons' Confluence interface to upload the jar file created in target/
- Search for the Macro Plugin in the available plugins.

