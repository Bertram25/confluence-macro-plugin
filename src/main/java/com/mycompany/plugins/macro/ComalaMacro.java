package com.mycompany.plugins.macro;

/// Confluence needed imports
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.xhtml.api.XhtmlContent;

import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;

import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.persistence.dao.compatibility.FindUserHelper;

/// Injections, Annotations, Auto-Importations
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;
import javax.inject.Inject;
import javax.inject.Named;

/// Comala Workflows
import com.comalatech.workflow.StateService;
import com.comalatech.workflow.model.State;
import com.comalatech.workflow.model.Approval;
/// Related to published vs latest page state.
import com.opensymphony.xwork.ActionContext;

/// Common imports
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;

/**
 * Comala Macro class.
 */
@Scanned
@Named ("ComalaMacro")
public class ComalaMacro implements Macro {

    /**
     * Confluence XHTML Content handler.
     */
    @ComponentImport
    private final XhtmlContent xhtmlUtils;

    /**
     * Confluence page handler.
     */
    @ComponentImport
    private PageManager pageManager;

    /**
     * Comala Workflows State service.
     */
    @ComponentImport
    private StateService stateService;

    /**
     * Constructor, initializing the XHTML Handler and state service through injection.
     * @param xhtmlUtils   XHTML content handler injected instance.
     * @param stateService The Comala state service injected instance.
     */
    @Inject
    public ComalaMacro(XhtmlContent xhtmlUtils,
                            StateService stateService) {
        this.xhtmlUtils = xhtmlUtils;
        this.stateService = stateService;
    }

    /**
     * Constructor, initializing the XHTML Handler and state service through injection.
     * @param params            The macro params, set in the macro edit screen.
     * @param bodyContent       The macro body content as string.
     * @param conversionContext The page conversion context,
     *                          containing the actual AbstractPage object.
     *
     * @throws MacroExecutionException If anything bad happens.
     *
     * @return The macro computed content as html string.
     */
    @Override
    public String execute(Map<String, String> params, String bodyContent, ConversionContext conversionContext) throws MacroExecutionException {
        // Obtain page instance
        // NOTE: Use draft to show something in preview for not yet created pages?
        AbstractPage page = (AbstractPage) (conversionContext.getPageContext()).getEntity();
        if (page == null) {
            return "<p><i>No available preview.</i></p>";
        }

        // Obtains current display state info.
        State currentState = getCurrentPageDisplayedState(page);
        String currentStateName = (currentState == null) ? null : currentState.getName();

        StringBuilder builder = new StringBuilder();
        if (currentStateName != null) {
            // Build the page status
            builder.append("<p><table class=\"confluenceTable\" width=\"50%\" border=\"1\">")
                   .append("<tr><th class=\"confluenceTh\">Status</th>")
                   .append("<td class=\"confluenceTd\"><b>").append(currentStateName).append("</b></td>")
                   .append("</tr></table></p>");
        }

        // Comala page state handler
        List states = (stateService == null) ? null : stateService.getStates(page);
        if (states == null || currentState == null) {
            return builder.toString();
        }

        Map<String, Date> approvers = new HashMap<String, Date>();

        for (Object state : states) {
            // Find people approving the document
            List<Approval> approvals = ((State) state).getApprovals();
            if (approvals == null) {
                continue;
            }

            for (Approval approval : approvals) {
                // Obtain FullName from username if possible.
                FindUserHelper userHelper = new FindUserHelper();
                String userFullName = approval.getUserName();
                ConfluenceUser user = userHelper.getUserByUsername(userFullName);
                if (user != null) {
                    userFullName = user.getFullName();
                }

                // N.B.: approval.GetName() contains the approval type.
                approvers.put(userFullName, approval.getDate());
            }
        }

        // Add each table rows.
        builder.append(computeActorsTableRow(approvers, "Approved by"));
        builder.append("</table></p>");

        //Debug help
        //builder.append(getDEBUGStatesListContent(states));
        //builder.append(getDEBUGParamsList(params));

        return builder.toString();
    }

    /**
     * Provides the page currently displayed state.
     * N.B.: Pages using Comala can potentially have two states:
     * The latest approved one, and the latest work in progress version.
     * @param page The current Confluence AbstractPage object.
     *
     * @return The currently displayed state, or null if anything fails..
     */
    private State getCurrentPageDisplayedState(AbstractPage page) {
        if (page == null || stateService == null) {
            return null;
        }

        // This is the magic used to get the currently displayed state.
        boolean isShowingPublishedState = ActionContext.getContext().getName().equalsIgnoreCase("releaseview");

        State currentState = isShowingPublishedState ?
                                stateService.getPublishedState(page) :
                                stateService.getCurrentState(page);

        return currentState;
    }

    /**
     * Creates the actors approval rows.
     * @param actors Map of actors and corresponding dates.
     * @param action The actor action. E.g.: "Approved by", ...
     *
     * @return The actor approval html table rows.
     */
    private String computeActorsTableRow(Map<String, Date> actors, String action) {
        if (actors == null || actors.isEmpty()) {
            return "";
        }

        StringBuilder builder = new StringBuilder();
        for (String actor : actors.keySet()) {
            Date signatureDate = actors.get(actor);

            builder.append("<tr><td class=\"confluenceTd\">").append(action).append("</td>")
                   .append("<td class=\"confluenceTd\">").append(actor).append("</td>")
                   .append("<td class=\"confluenceTd\">").append(signatureDate)
                   .append("</td></tr>");
        }
        return builder.toString();
    }

    /**
     * Returns the states names and dates in an html table row for debugging purpose.
     * @param states List of Comala Workflows states.
     *
     * @return The states names and dates in an html table rows.
     */
    private String getDEBUGStatesListContent(List states) {
        if (states == null) {
            return "";
        }

        StringBuilder builder = new StringBuilder();
        builder.append("<table><tr><th>State Name</th><th>Date</th></tr>");
        for (Object stateObj : states) {
            State state = (State) stateObj;
            builder.append("<tr><td>").append(state.getName()).append("</td><td>")
                   .append(state.getDate()).append("</td></tr>");
        }
        builder.append("</table>");
        return builder.toString();
    }

    /**
     * Returns the macro parameters in an html table row for debugging purpose.
     * @param params Map of parameter key and coresponding values.
     *
     * @return The parameters keys and values in an html table rows.
     */
    private String getDEBUGParamsList(Map<String, String> params) {
        if (params == null) {
            return "";
        }

        StringBuilder builder = new StringBuilder();
        builder.append("<p><table><tr><th>key</th><th>value</th></tr>");
        for (String param : params.keySet()) {
            String paramKey = params.get(param);
            builder.append("<tr><td>").append(paramKey).append("</td><td>")
                   .append(param).append("</td></tr>");
        }
        builder.append("</table></p>");
        return builder.toString();
    }

    /**
     * Returns the macro body type.
     * @return Confluence macro body type.
     */
    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    /**
     * Returns the macro output type.
     * @return Confluence macro output type.
     */
    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}
